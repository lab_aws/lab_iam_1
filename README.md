# Small ACG Labs

ACG Labs performed with Terraform

## Pre-Requisites
* AWS CLI configured and working
* Default profile is "acg_lab" - can be changed in `main.tf`


## Introduction to AWS Identity and Access Management (IAM)

* [Link](https://learn.acloud.guru/handson/4b620748-f44f-408a-a42b-f727a208e952)
