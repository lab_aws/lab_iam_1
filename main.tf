terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  shared_config_files      = ["~/.aws/config"]
  shared_credentials_files = ["~/.aws/credentials"]
  profile                  = "acg_lab"
}


resource "aws_iam_group" "S3-Support" {
    name = "S3-Support"
}
resource "aws_iam_group" "EC2-Support" {
    name = "EC2-Support"
}
resource "aws_iam_group" "EC2-Admin" {
    name = "EC2-Admin"
}

resource "aws_iam_user" "user-1" {
    name = "user-1"
}

resource "aws_iam_user" "user-2" {
    name = "user-2"
}

resource "aws_iam_user" "user-3" {
    name = "user-3"
}


resource "aws_iam_user_group_membership" "user-1" {
  user = aws_iam_user.user-1.name

  groups = [
    aws_iam_group.S3-Support.name
  ]

}

resource "aws_iam_user_group_membership" "user-2" {
  user = aws_iam_user.user-2.name

  groups = [
    aws_iam_group.EC2-Support.name
  ]

}

resource "aws_iam_user_group_membership" "user-3" {
  user = aws_iam_user.user-3.name

  groups = [
    aws_iam_group.EC2-Admin.name
  ]

}