all: init import

init:
	terraform init

import:
	for u in  user-(seq 1 3) ; do terraform import aws_iam_user.$u $u ; done
	for g in  "S3-Support" "EC2-Support" "EC2-Admin" ; terraform import aws_iam_group.$g $g ; done